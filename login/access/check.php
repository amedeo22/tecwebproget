<?php

include './../../html/utilities/functions.php';
include './../../html/utilities/db_connect.php';

  sec_session_start(); // usiamo la nostra funzione per avviare una sessione php sicura
  if(isset($_POST['email'], $_POST['pwd'])) {
     $email = $_POST['email'];
     $password = $_POST['pwd']; // Recupero la password criptata.
     $mysqli = connectToDatabase();

     if ($mysqli->connect_error) {
       header('Location: ./login.php?error=4');
       exit;
     }

     /* controllo che l'emails sia già presente*/
     $res = checkEmail($email,$mysqli);
     if (!$res) {
       header('Location: ./login.php?error=0');
       exit;
     }

     $res = login($email, $password, $mysqli);
     if($res === 1) {
        // Login eseguito da non fornitore
        setClientData($mysqli, $email);
        if(isset($_SESSION["lastPage"]) && ($_SESSION["num"])==1) {
          header('Location: ./../../html/'.$_SESSION["lastPage"]);
        } else {
          header('Location: ./../../html/ricerca.php');
        }
     } else if ($res === 2){
       //Login eseguito da fornitore
       setProviderData($mysqli, $email);
       if(isset($_SESSION["lastPage"]) && ($_SESSION["num"])==2) {
         header('Location: ./../../html/'.$_SESSION["lastPage"]);
       } else {
         header('Location: ./../../html/fornitore_menu.php');
       }
     }else if($res === 3){
       //Login eseguito da fornitore
       setAdminData($mysqli, $email);
       if(isset($_SESSION["lastPage"]) && ($_SESSION["num"])==3) {
         header('Location: ./../../html/'.$_SESSION["lastPage"]);
       } else {
         header('Location: ./../../html/Admin_lista_fornitori.php');
       }
     } else {
        // Login fallito
       header('Location: ./login.php?error='.$res);
     }
  } else if ((isset($_POST['email']) && strcmp($_POST['email'],"") == 0) || (!isset($_POST['email'])) ) {
      header('Location: ./login.php?error=2');
  } else if (!isset($_POST['pwd'])) {
      header('Location: ./login.php?error=3');
  } else {
      // Le variabili corrette non sono state inviate a questa pagina dal metodo POST.
      echo 'Invalid Request';
  }

?>
